/*
1. [x] Display the location for each move in the format (col, row) in the move
   history list.
2. [x] Bold the currently selected item in the move list.
3. [x] Rewrite Board to use two loops to make the squares instead of hardcoding
   them. NOTE: refactored to use one loop & .map
4. [x] Add a toggle button that lets you sort the moves in either ascending or
   descending order.
5. [x] When someone wins, highlight the three squares that caused the win.
6. [x] When no one wins, display a message about the result being a draw.
Extra:
7. [x] Add button to restart game
*/

import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

function calculateWinner(squares) {
  const winning_lines = [
    [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < winning_lines.length; i++) {
    const a = squares[winning_lines[i][0]];
    if (a && winning_lines[i].every((e) => squares[e] === a)) {
      return winning_lines[i];
    }
  }
  return [];
}

function Square(props) {
  return (
    <button style={props.style} className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        key={i}
        style={this.props.win.indexOf(i) >= 0 ? { color: "red" } : {}}
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    let rows = [];
    for (let i = 0; i < 3; i++) {
      const columns = [0, 1, 2].map(j => this.renderSquare(3 * i + j))
      rows.push(<div key={i} className="board-row">{columns}</div>);
    }
    return <div>{rows}</div>;
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.initial = {
      history: [{ squares: Array(9).fill(null), position: null }],
      stepNumber: 0,
      xIsNext: true,
      descending: false,
    };
    this.state = this.initial;
  }

  resetGame() {
    const rst = () => this.setState(this.initial);
    return <p><button onClick={rst}>New game</button></p>;
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    if (calculateWinner(squares).length !== 0 || squares[i]) { return; }

    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: history.concat([{ squares: squares, position: i }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) { this.setState({ stepNumber: step, xIsNext: step % 2 === 0 }); }

  toggleHistory() {
    const tgl = () => { this.setState({ descending: !this.state.descending }) };
    return <button onClick={tgl}> Toggle history </button>;
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];

    // Description block
    // step -> whole object, move -> index in history Array
    const moves = history.map((step, move) => {
      const pos = step.position;
      let desc = move
        ? `Go to move #${move} (${Math.floor(pos / 3) + 1}, ${(pos % 3) + 1})`
        : "Go to game start";

      if (move === this.state.stepNumber) { desc = <b>{desc}</b>; }

      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    if (this.state.descending) { moves.reverse(); }

    // Status block
    let status;
    const result = calculateWinner(current.squares);
    if (result.length !== 0) {
      status = `Winner: ${current.squares[result[0]]}!`;
    } else if (history.some((e) => e.squares.every((x) => x))) {
      status = "Draw.";
    } else {
      status = "Next player: " + (this.state.xIsNext ? "X" : "O");
    }

    return (
      <div className="game">
        <div>{this.resetGame()}</div>
        <div className="game-board">
          <Board
            win={result}
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <p>{this.toggleHistory()}</p>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Game />, document.getElementById("root"));
